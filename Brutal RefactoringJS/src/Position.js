/**
 * Created by NStirzak on 24/03/14.
 */
var Position = function(row,col){
    var _row = row;
    var _col = col;
    return {
        row : _row,
        col : _col
    }
};

var LEFT_COLUMN = 0;
var MIDDLE_COLUMN = 1;
var RIGHT_COLUMN = 2;

var TOP_ROW = 0;
var MIDDLE_ROW = 1;
var BOTTOM_ROW = 2;

var TOP_LEFT = new Position(TOP_ROW, LEFT_COLUMN);
var TOP_MIDDLE = new Position(TOP_ROW, MIDDLE_COLUMN);
var TOP_RIGHT = new Position(TOP_ROW, RIGHT_COLUMN);

var MIDDLE_LEFT = new Position(MIDDLE_ROW, LEFT_COLUMN);
var MIDDLE_MIDDLE = new Position(MIDDLE_ROW, MIDDLE_COLUMN);
var MIDDLE_RIGHT = new Position(MIDDLE_ROW, RIGHT_COLUMN);

var BOTTOM_LEFT = new Position(BOTTOM_ROW, LEFT_COLUMN);
var BOTTOM_MIDDLE = new Position(BOTTOM_ROW, MIDDLE_COLUMN);
var BOTTOM_RIGHT = new Position(BOTTOM_ROW, RIGHT_COLUMN);

