/**
 * Created by NStirzak on 24/03/14.
 */
var Grid = function(){
    var cells = [];

    this.isEmpty = function(){
        return cells.length === 0;
    };

    this.getCell = function(position){
        return cells[position] || CellValue.EMPTY;
    };

    this.setCell = function(position, value) {
        if(this.getCell(position) != CellValue.EMPTY) {
            console.log(this.getCell(position));

            throw 'AlreadySetException';
        }
        cells[position] = value;
    };
};