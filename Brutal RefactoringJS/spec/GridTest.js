describe("grid", function() {
    var grid;

    beforeEach(function() {
        grid = new Grid();
    });

    it("Grid is empty", function() {
        expect(grid.isEmpty()).toBeTruthy();
    });

    it('can get value',function(){
        var position = TOP_LEFT;
        expect( grid.getCell(position)).toBe(CellValue.EMPTY);
    });

    it('can set value',function(){
        var position = TOP_LEFT;
        grid.setCell(position,CellValue.TICK);
        expect( grid.getCell(position)).toBe(CellValue.TICK);
    });

    it('cannot set an already set position', function() {
        var position = TOP_LEFT;
        grid.setCell(position, CellValue.TICK);
        expect(grid.setCell(position, CellValue.CROSS)).toThrow();
    });
});